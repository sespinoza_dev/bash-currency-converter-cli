# README

## Description

Simple Bash Script that makes an http request to an API currency converter.

## Deploy

To use it as a command line interfase (CLI) just add it to the path.

```console
$ mkdir -p ~/.local/bin/
$ cp converter ~/.local/bin/converter
```

Open a new terminal and try it:

```console
$ converter 4
 4  Euros correspond to  4.428584  USD dollars
```

## Contributors

- [sespinoz](https://gitlab.com/sespinoz) [@Sam](sespinoza@zeroq.cl) - creator, maintainer
